package com.phonecompany.billing;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class TelephoneBillCalculatorImpl implements TelephoneBillCalculator {

    /**
     * @param phoneLog - string with calls separated with \n, call attributes separated by ,
     * @return total price
     * Calculates total price of these calls
     */
    public BigDecimal calculate(String phoneLog) {
        List<Call> callList = new ArrayList<>();
        HashMap<Long, Integer> callsToThisNumberAmount = new HashMap<>();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

        for (String logLine : phoneLog.split("\n")) {
            String[] callParameters = logLine.split(",");
            long number = Long.parseLong(callParameters[0]);
            LocalDateTime callStart = LocalDateTime.parse(callParameters[1], formatter);
            LocalDateTime callEnd = LocalDateTime.parse(callParameters[2], formatter);

            callList.add(new Call(number, callStart, callEnd));
            int thisNumberCallsAmount = callsToThisNumberAmount.getOrDefault(number, 0);
            callsToThisNumberAmount.put(number, thisNumberCallsAmount + 1);
        }
        long maxCallsNumber = findMostCalledNumber(callsToThisNumberAmount);

        double price = callList.stream().filter(o -> o.number != maxCallsNumber).mapToDouble(Call::calculatePrice).sum();
        BigDecimal priceBigDecimal = new BigDecimal(price);
        priceBigDecimal = priceBigDecimal.setScale(2, RoundingMode.HALF_UP);
        return priceBigDecimal;
    }

    /**
     * @param callsAmount - map of numbers as key and amount of this number usage as value
     * @return the most frequent number
     * Finding the most frequent number in map using rules provided in task. If there are several of them chooses the biggest number 
     */
    public long findMostCalledNumber(HashMap<Long, Integer> callsAmount) {
        int maxCallsAmount = 0;
        long maxCallsNumber = 0;
        for (Map.Entry<Long, Integer> callAmount : callsAmount.entrySet()) {
            if (callAmount.getValue() > maxCallsAmount ||
                    (callAmount.getValue() == maxCallsAmount && maxCallsNumber < callAmount.getKey())) {
                maxCallsAmount = callAmount.getValue();
                maxCallsNumber = callAmount.getKey();
            }
        }
        return maxCallsNumber;
    }

    /**
     * Inner class Call to store call attributes and calculate price for one call
     */
    private static class Call {

        private final long number;
        private final LocalDateTime start;
        private final LocalDateTime end;

        public Call(long number, LocalDateTime start, LocalDateTime end) {
            this.number = number;
            this.start = start;
            this.end = end;
        }

        /**
         * @return priceOfThisCall
         * Calculates price of this call
         */
        public double calculatePrice() {
            Duration duration = Duration.between(start, end);
            long minutes = duration.toMinutes();
            if(duration.toSeconds() > minutes * 60) minutes += 1;
            long minutesAfterFiveMinutes = minutes - 5;
            if(minutesAfterFiveMinutes < 0) minutesAfterFiveMinutes = 0;
            long highPriceMinutes = 0;
            long lowPriceMinutes = 0;
            if ((start.getHour() < 7 && start.getMinute() < 55) || start.getHour() >= 16) { // first 5 minutes cost low price
                lowPriceMinutes = minutes - minutesAfterFiveMinutes;
            } else if(start.getHour() == 7){ // first part of first 5 minutes costs low price and second part costs high price
                lowPriceMinutes = 60 - start.getMinute();
                if(lowPriceMinutes > 5) lowPriceMinutes = 5;
                highPriceMinutes = minutes - lowPriceMinutes - minutesAfterFiveMinutes;
                if(highPriceMinutes < 0) highPriceMinutes = 0;
            } else if(start.getHour() == 15){ // first part of first 5 minutes costs high price and second part costs low price
                highPriceMinutes = 60 - start.getMinute();
                if(highPriceMinutes > 5) highPriceMinutes = 5;
                lowPriceMinutes = minutes - highPriceMinutes - minutesAfterFiveMinutes;
                if(lowPriceMinutes < 0) lowPriceMinutes = 0;
            } else {
                highPriceMinutes = minutes - minutesAfterFiveMinutes; // first 5 minutes cost high price
            }
            return minutesAfterFiveMinutes * 0.2 + highPriceMinutes + lowPriceMinutes * 0.5;
        }
    }
}
