package com.phonecompany.billing;

import java.math.BigDecimal;

public interface TelephoneBillCalculator {
    /**
     * @param phoneLog - string with calls separated with \n, call attributes separated by ,
     * Calculates total price of these calls
     * @return total price
     */
    BigDecimal calculate (String phoneLog);
}