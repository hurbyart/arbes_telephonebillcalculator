package com.phonecompany.billing;

import org.junit.jupiter.api.Test;


import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class TelephoneBillCalculatorTest {

    @Test
    public void onlyLowCostCalls(){
        // arrange
        BigDecimal expectedPrice = new BigDecimal("4.70");
        String data = "420774577453,13-01-2020 18:10:15,13-01-2020 18:13:57\n" +
                      "420776562353,18-01-2020 20:56:20,18-01-2020 21:07:00\n" +
                      "420776562353,19-01-2020 08:59:20,19-01-2020 09:10:00\n" +
                      "420776599354,19-01-2020 23:58:20,20-01-2020 00:04:10";

        //act
        TelephoneBillCalculator calculator = new TelephoneBillCalculatorImpl();
        BigDecimal realPrice = calculator.calculate(data);

        //assert
        assertEquals(expectedPrice, realPrice);
    }

    @Test
    public void onlyHighCostCalls(){
        // arrange
        BigDecimal expectedPrice = new BigDecimal("15.40");
        String data = "420774577453,13-01-2020 08:10:15,13-01-2020 08:13:57\n" +
                      "420776562353,18-01-2020 12:56:20,18-01-2020 12:07:00\n" +
                      "420776562353,19-01-2020 13:59:20,19-01-2020 13:10:00\n" +
                      "420778562363,19-01-2020 10:59:20,19-01-2020 11:10:00\n" +
                      "420776599354,19-01-2020 14:58:20,19-01-2020 15:04:10";

        //act
        TelephoneBillCalculator calculator = new TelephoneBillCalculatorImpl();
        BigDecimal realPrice = calculator.calculate(data);

        //assert
        assertEquals(expectedPrice, realPrice);
    }

    @Test
    public void callsWithStartWithLowPriceAndEndWithHighPrice(){
        // arrange
        BigDecimal expectedPrice = new BigDecimal("15.20");
        String data = "420774577453,13-01-2020 07:50:15,13-01-2020 08:13:57\n" +
                      "420776562353,18-01-2020 07:56:20,18-01-2020 08:07:00\n" +
                      "420776562353,19-01-2020 07:59:20,19-01-2020 08:10:00\n" +
                      "420778562363,19-01-2020 07:58:20,19-01-2020 08:08:00\n" +
                      "420776599354,19-01-2020 07:57:10,19-01-2020 08:04:10";

        //act
        TelephoneBillCalculator calculator = new TelephoneBillCalculatorImpl();
        BigDecimal realPrice = calculator.calculate(data);

        //assert
        assertEquals(expectedPrice, realPrice);
    }

    @Test
    public void callsWithStartWithHighPriceAndEndWithLowPrice(){
        // arrange
        BigDecimal expectedPrice = new BigDecimal("17.70");
        String data = "420774577453,13-01-2020 15:50:15,13-01-2020 16:13:57\n" +
                      "420776562353,18-01-2020 15:56:20,18-01-2020 16:07:00\n" +
                      "420776562353,19-01-2020 15:59:20,19-01-2020 16:10:00\n" +
                      "420778562363,19-01-2020 15:58:20,19-01-2020 16:08:00\n" +
                      "420776599354,19-01-2020 15:57:10,19-01-2020 16:04:10";

        //act
        TelephoneBillCalculator calculator = new TelephoneBillCalculatorImpl();
        BigDecimal realPrice = calculator.calculate(data);

        //assert
        assertEquals(expectedPrice, realPrice);
    }

    @Test
    public void differentCalls(){
        // arrange
        BigDecimal expectedPrice = new BigDecimal("22.30");
        String data = "420774577453,13-01-2020 15:50:15,13-01-2020 16:13:57\n" +
                      "420776562353,18-01-2020 15:56:20,18-01-2020 16:07:00\n" +
                      "420776562353,19-01-2020 15:59:20,19-01-2020 16:10:00\n" +
                      "420778562363,19-01-2020 07:58:20,19-01-2020 08:08:00\n" +
                      "420778562123,19-01-2020 05:38:20,19-01-2020 05:47:00\n" +
                      "420776599354,19-01-2020 13:23:10,19-01-2020 13:29:10";

        //act
        TelephoneBillCalculator calculator = new TelephoneBillCalculatorImpl();
        BigDecimal realPrice = calculator.calculate(data);

        //assert
        assertEquals(expectedPrice, realPrice);
    }
}